<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Usuarios_model");

        
    }
	
	public function index()
	{
        if ($this->session->userdata("login")) {
            redirect(base_url()."dashboard");

        }else {
            $this->load->view('admin/login');
        }
		
    }


    public function login(){
        
        $usuario=$this->input->post("usuario");
        $pass=$this->input->post("pass");
        

        $ress = $this->Usuarios_model->login($usuario,$pass);
       

        if (!$ress) 
        {
            redirect(base_url());
        }
        else{
            $dato = array(
                'id_usr'=> $ress->id_usr,
                'nombre'=>$ress->nombre,
                'id_empresa'=>$ress->id_empresa,
                'id_cargo'=>$ress->id_cargo,
                'estado'=>$ress->estado,
                'login'=> TRUE
            );

           
            $this->session->set_userdata($dato);
            redirect(base_url()."dashboard");

        }

    }
    
}
